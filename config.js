window.config = {
    services: {
        configuration: CONFIG_HOST
    }
};

var fetch = function (request) {
    GET_TOKEN
    window.dispatchEvent(new Event("appTokenLoaded"));
}

Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
 
// loading the request script
var src = "";
if (document.location.hostname == "192.168.0.56") {
    src = "//192.168.0.56/v1/request/read?callback=fetch";
} else {
    var prefix = (document.location.hostname.indexOf("uat") == 0 ? "uat." : "");
    src = "//" + prefix + "api.lendfoundry.com/v1/request/read?callback=fetch"
}

var script = document.createElement('script');
script.src = src;
script.async = false;
document.head.appendChild(script);

