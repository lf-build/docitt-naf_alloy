// Karma configuration
// http://karma-runner.github.io/0.12/config/configuration-file.html
// Generated on 2015-08-04 using
// generator-karma 1.0.0

module.exports = function(config) {
  'use strict';

  config.set({
    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,

    // base path, that will be used to resolve files and exclude
    basePath: '../',

    // testing framework to use (jasmine/mocha/qunit/...)
    // as well as any additional frameworks (requirejs/chai/sinon/...)
    frameworks: [
      "jasmine"
    ],

    // list of files / patterns to load in the browser
    files: [
      // bower:js
      'bower_components/jquery/dist/jquery.js',
      'bower_components/autoNumeric/autoNumeric.js',
      'bower_components/bootstrap/dist/js/bootstrap.js',
      'bower_components/bootstrap-datepicker/js/bootstrap-datepicker.js',
      'bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.js',
      'bower_components/datatables/media/js/jquery.dataTables.js',
      'bower_components/dropzone/dist/min/dropzone.min.js',
      'bower_components/jquery-validation/dist/jquery.validate.js',
      'bower_components/metisMenu/dist/metisMenu.js',
      'bower_components/PACE/pace.js',
      'bower_components/webcomponentsjs/webcomponents.js',
      'bower_components/slimScroll/jquery.slimscroll.min.js',
      'bower_components/toastr/toastr.js',
      'bower_components/moment/moment.js',
      'bower_components/datatables-buttons/js/dataTables.buttons.js',
      'bower_components/datatables-buttons/js/buttons.colVis.js',
      'bower_components/datatables-buttons/js/buttons.flash.js',
      'bower_components/datatables-buttons/js/buttons.html5.js',
      'bower_components/datatables-buttons/js/buttons.print.js',
      'bower_components/jszip/dist/jszip.js',
      'bower_components/pdfmake/build/pdfmake.js',
      'bower_components/pdfmake/build/vfs_fonts.js',
      'bower_components/jquery-mask-plugin/dist/jquery.mask.js',
      'bower_components/jquery.steps/build/jquery.steps.js',
      // endbower
      "app/scripts/**/*.js",
      "test/mock/**/*.js",
      "test/spec/**/*.js"
    ],

    // list of files / patterns to exclude
    exclude: [
    ],

    // web server port
    port: 8080,

    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera
    // - Safari (only Mac)
    // - PhantomJS
    // - IE (only Windows)
    browsers: [
      "PhantomJS"
    ],

    // Which plugins to enable
    plugins: [
      "karma-phantomjs-launcher",
      "karma-jasmine"
    ],

    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false,

    colors: true,

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_INFO,

    // Uncomment the following lines if you are using grunt's server to run the tests
    // proxies: {
    //   '/': 'http://localhost:9000/'
    // },
    // URL root prevent conflicts with the site root
    // urlRoot: '_karma_'
  });
};
