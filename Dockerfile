FROM node:slim

RUN apt-get update && apt-get install -y git git-core

WORKDIR /src

RUN npm install -g grunt-cli bower git

ADD package.json /src/package.json

RUN npm install

ADD config.js /src/config.js
ADD Gruntfile.js /src/Gruntfile.js
ADD bower.json /src/bower.json
ADD .bowerrc /src/.bowerrc

RUN bower install --allow-root



ADD ./app /src/app

WORKDIR /src

RUN grunt --force build

EXPOSE 9002

ENTRYPOINT grunt --force dist
